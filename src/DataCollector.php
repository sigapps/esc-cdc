<?php
namespace Esc\CDC;

use Esc\CDC\API;
use Illuminate\Database\Eloquent\Model;

trait CentralDataCollector {
    protected abstract function getType();
    protected abstract function getIdentifier();


    public function syncData(Model $model, $force = true) {
        $data = [
            'model_type' => $this->getType(),
            'model_id' => $this->getIdentifier(),
            'data' => []
        ];

        if ($force) {
            foreach ($model->attributes as $key => $value) {
                $data['data'][$key] = $value;
            }
        } else {
            foreach ($model->getDirty() as $key => $value) {
                $data['data'][$key] = $value;
            }
        }

        API::sendData($data);
    }
}
